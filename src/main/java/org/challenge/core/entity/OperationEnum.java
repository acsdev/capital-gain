package org.challenge.core.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public enum OperationEnum {
    BUY,
    SELL;

    @JsonValue
    public String get() {
        return this.name().toLowerCase();
    }
}