package org.challenge.core.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Represents input data model
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "operation")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SellOperation.class, name = "sell"),
        @JsonSubTypes.Type(value = BuyOperation.class, name = "buy")
})
public class Operation {

    private final OperationEnum operation;

    private final double unitCost;

    private final long quantity;

    @JsonCreator
    public Operation(@JsonProperty("operation") OperationEnum operation,
                     @JsonProperty("unit-cost") double unitCost,
                     @JsonProperty("quantity") long quantity) {
        this.operation = operation;
        this.unitCost = unitCost;
        this.quantity = quantity;
    }

    public OperationEnum getOperation() {
        return operation;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public long getQuantity() {
        return quantity;
    }

    public double getTotal() {
        return this.unitCost * this.quantity;
    }

    public boolean isPurchase() {
        return OperationEnum.BUY.equals(this.operation);
    }

    public BuyOperation castBuyOperation() {
        return (BuyOperation) this;
    }
    public SellOperation castSellOperation() {
        return (SellOperation) this;
    }
}