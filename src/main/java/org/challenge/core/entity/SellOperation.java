package org.challenge.core.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.List;

@JsonTypeName("SELL")
public class SellOperation extends  Operation {

    @JsonCreator
    public SellOperation(@JsonProperty("unit-cost") double unitCost,
                        @JsonProperty("quantity") long quantity) {
        super(OperationEnum.SELL, unitCost, quantity);
    }

    public double getTotalValue() {
        return this.getUnitCost() * this.getQuantity();
    }

    public double getPurchaseTotal(double averagePurchasePrice) {
        return this.getQuantity() * averagePurchasePrice;
    }

    public boolean hasGain(double averagePurchasePrice) {
        double saleTotalValue = this.getTotalValue();
        return saleTotalValue >= averagePurchasePrice;
    }

    public double getGain(double averagePurchasePrice, double currentLoss) {
        double purchaseTotal = this.getPurchaseTotal(averagePurchasePrice);
        double saleTotalValue = this.getTotalValue();
        return saleTotalValue -  purchaseTotal - currentLoss;
    }

    public double getLoss(double averagePurchasePrice) {
        double saleTotalValue = this.getTotalValue();
        double purchaseTotal  = this.getPurchaseTotal(averagePurchasePrice);
        return saleTotalValue - purchaseTotal;
    }
}
