package org.challenge.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.challenge.core.control.DoubleSerializer;

/**
 * Represent the tex of one operation
 */
public class OperationResult {

    private final Double tax;

    private final String error;

    private OperationResult(Builder builder) {
        this.tax = builder.tax;
        this.error = builder.error;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonSerialize(using = DoubleSerializer.class)
    public Double getTax() {
        return tax;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getError() {
        return error;
    }

    public static final class Builder {
        private Double tax;
        private String error;

        public Builder() {
        }

        public Builder tax(double val) {
            tax = val;
            return this;
        }

        public Builder error(String error) {
            this.tax = null;
            this.error = error;
            return this;
        }
        public OperationResult build() {
            return new OperationResult(this);
        }
    }
}
