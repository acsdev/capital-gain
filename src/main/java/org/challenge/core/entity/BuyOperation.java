package org.challenge.core.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("BUY")
public class BuyOperation extends Operation {

    @JsonCreator
    public BuyOperation(@JsonProperty("unit-cost") double unitCost,
                        @JsonProperty("quantity") long quantity) {
        super(OperationEnum.BUY, unitCost, quantity);
    }
}
