package org.challenge.core.control;

import org.challenge.core.entity.Operation;
import org.challenge.core.entity.OperationResult;

import java.util.List;

public interface IMapper {

    List<Operation> getOperationList(String input);

    String parse(List<OperationResult> results);
}
