package org.challenge.core.control;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class DoubleSerializer extends JsonSerializer<Double> {

    @Override
    public void serialize(Double value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        if (value == null || value == 0) {
            gen.writeNumber(0);
            return;
        }
        if (value % 1 == 0) {
            gen.writeNumber(value.longValue());
            return;
        }
        gen.writeNumber(value);
    }
}
