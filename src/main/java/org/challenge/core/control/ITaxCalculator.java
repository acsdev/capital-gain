package org.challenge.core.control;

import org.challenge.core.entity.BuyOperation;
import org.challenge.core.entity.Operation;
import org.challenge.core.entity.SellOperation;

import java.util.List;

public interface ITaxCalculator {

    double getTax(SellOperation salesQuantity, List<BuyOperation> operationList);
}
