package org.challenge.core.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import org.challenge.core.entity.Operation;
import org.challenge.core.entity.OperationResult;

import java.util.LinkedList;
import java.util.List;

public class Mapper implements IMapper {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public List<Operation> getOperationList(String input) {
        CollectionLikeType type = objectMapper.getTypeFactory().constructCollectionLikeType(List.class, Operation.class);
        try {
            return new LinkedList<>(objectMapper.readValue(input, type));
        } catch (JsonProcessingException e) {
            throw new InvalidContentException(e.getMessage());
        }
    }

    @Override
    public String parse(List<OperationResult> results) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(results);
        } catch (JsonProcessingException e) {
            throw new InvalidContentException(e.getMessage());
        }
    }
}
