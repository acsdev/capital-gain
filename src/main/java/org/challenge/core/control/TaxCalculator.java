package org.challenge.core.control;

import org.challenge.core.entity.BuyOperation;
import org.challenge.core.entity.Operation;
import org.challenge.core.entity.SellOperation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static java.util.stream.Collectors.toList;
public class TaxCalculator implements ITaxCalculator {

    public static final double ZERO = BigDecimal.ZERO.doubleValue();

    private double currentLoss = ZERO;

    @Override
    public double getTax(SellOperation sell, List<BuyOperation> buyList) {
        final double averagePurchasePrice = this.getAveragePurchasePrice(buyList);
        boolean hasGain = sell.hasGain(averagePurchasePrice);
        if (hasGain) {
            final double gain = sell.getGain(averagePurchasePrice, this.currentLoss);
            if (gain <= 0) {
                this.currentLoss = Math.abs(gain);
            } else if (gain >= 2000) {
                return gain * 20 / 100;
            }
        } else {
            this.currentLoss += sell.getLoss(averagePurchasePrice);
        }
        return ZERO;
    }

    /**
     * Calculate average purchase price
     *
     * @param purchases list of operation
     * @return average purchase price
     */
    private double getAveragePurchasePrice(List<BuyOperation> purchases) {
        double total = purchases.stream().mapToDouble(Operation::getTotal).sum();
        double quantity = purchases.stream().mapToDouble(Operation::getQuantity).sum();
        double averagePurchasePrice = total / quantity;
        return round(averagePurchasePrice);
    }

    /**
     * Round double value for two decimals
     *
     * @param value double
     * @return rounded double
     */
    private double round(double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
