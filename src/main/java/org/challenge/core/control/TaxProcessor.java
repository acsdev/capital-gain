package org.challenge.core.control;

import org.challenge.core.entity.BuyOperation;
import org.challenge.core.entity.Operation;
import org.challenge.core.entity.OperationResult;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class TaxProcessor {

    private static final double ZERO = BigDecimal.ZERO.doubleValue();
    public static final String CANT_SELL_MORE_STOCKS_THAN_YOU_HAVE = "Can't sell more stocks than you have";

    private final IMapper mapper;

    private final ITaxCalculator calculator;

    public TaxProcessor(IMapper mapper, ITaxCalculator calculator) {
        this.mapper = mapper;
        this.calculator = calculator;
    }

    public String execute(String input) {
        final List<Operation> operationList = this.mapper.getOperationList(input);
        final List<OperationResult> operationResultList = new ArrayList<>(operationList.size());

        int currentQuantity = 0;
        final List<BuyOperation> purchaseOperationList = new ArrayList<>();
        for (Operation operation : operationList) {

            if (operation.isPurchase()) {
                currentQuantity += operation.getQuantity();
                purchaseOperationList.add(operation.castBuyOperation());
                operationResultList.add(this.getOperationResult(ZERO));
                continue;
            }

            final long salesQuantity = operation.getQuantity();
            if (salesQuantity > currentQuantity) {
                operationResultList.add(this.getOperationResult(CANT_SELL_MORE_STOCKS_THAN_YOU_HAVE));
                continue;
            }

            currentQuantity -= salesQuantity;
            final double tax = this.calculator.getTax(operation.castSellOperation(), purchaseOperationList);
            operationResultList.add(this.getOperationResult(tax));
        }

        return mapper.parse(operationResultList);
    }

    private OperationResult getOperationResult(double value) {
        return new OperationResult.Builder().tax(value).build();
    }

    private OperationResult getOperationResult(String value) {
        return new OperationResult.Builder().error(value).build();
    }

    private int getPurchaseQuantity(List<Operation> operationList) {
        List<Operation> purchases = operationList.stream().filter(Operation::isPurchase).collect(toList());
        return (int) purchases.stream().mapToDouble(Operation::getQuantity).sum();
    }

    /**
     * Calculate average purchase price
     *
     * @param operationList list of operation
     * @return average purchase price
     */
    private double getAveragePurchasePrice(List<Operation> operationList) {
        List<Operation> purchases = operationList.stream().filter(Operation::isPurchase).collect(toList());
        double total = purchases.stream().mapToDouble(Operation::getTotal).sum();
        double quantity = purchases.stream().mapToDouble(Operation::getQuantity).sum();
        double averagePurchasePrice = total / quantity;
        return round(averagePurchasePrice);
    }

    /**
     * Round double value for two decimals
     *
     * @param value double
     * @return rounded double
     */
    private double round(double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
