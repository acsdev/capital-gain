package org.challenge.core.boundary;

import org.challenge.core.control.ContentNotFoundException;
import org.challenge.core.control.Mapper;
import org.challenge.core.control.TaxCalculator;
import org.challenge.core.control.TaxProcessor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static java.lang.String.format;

public class App {

    public static void main(String[] args) {

        final var filePath = args[0];
        if (filePath == null || filePath.isBlank()) {
            throw new IllegalArgumentException("File parameter was not found");
        }

        final var content = getContent(filePath);
        content.forEach(input -> {
            TaxProcessor taxProcessor = new TaxProcessor(new Mapper(), new TaxCalculator());
            String output = taxProcessor.execute(input);
            System.out.printf(" INPUT: %s%n", input);
            System.out.printf("OUTPUT: %s%n", output);
            System.out.println();
        });
    }

    private static List<String> getContent(String filePath) {
        try {
            return Files.readAllLines(Path.of(filePath), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new ContentNotFoundException(format("Error to read data from %s", filePath));
        }
    }
}
