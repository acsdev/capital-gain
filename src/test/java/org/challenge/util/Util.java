package org.challenge.util;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

public class Util {

    public static String getFileFromResource(String fileName) {
        ClassLoader classLoader = Util.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        }
        try {
            return Files.readString(Path.of(resource.toURI()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}
