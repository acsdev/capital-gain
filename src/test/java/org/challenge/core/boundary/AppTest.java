package org.challenge.core.boundary;

import org.challenge.core.control.ContentNotFoundException;
import org.challenge.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;

class AppTest {

    @Test
    void shouldFailDueToEmptyInput() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> App.main(new String[]{""}));
    }

    @Test
    void shouldFailDueToInvalidFile() {
        Assertions.assertThrows(ContentNotFoundException.class, () -> App.main(new String[]{"/file"}));
    }

    @Test
    void shouldWorkFine() {
        ClassLoader classLoader = Util.class.getClassLoader();
        String input = Objects.requireNonNull(classLoader.getResource("test-cases/1-2.in")).getPath();
        Assertions.assertDoesNotThrow(() -> App.main(new String[]{input}));
    }
}