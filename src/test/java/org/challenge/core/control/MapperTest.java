package org.challenge.core.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.challenge.core.entity.Operation;
import org.challenge.core.entity.OperationEnum;
import org.challenge.core.entity.OperationResult;
import org.challenge.util.Util;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MapperTest {

    Mapper mapper = new Mapper();

    @Test
    void shouldDeserializeList() throws JsonProcessingException {
        String json = Util.getFileFromResource("operation-list-sample.json");

        List<Operation> operations = mapper.getOperationList(json);
        assertEquals(2, operations.size());
        Operation first = operations.get(0);
        Operation second = operations.get(1);
        assertEquals(OperationEnum.BUY, first.getOperation());
        assertEquals(10.0d, first.getUnitCost());
        assertEquals(10000L, first.getQuantity());
        assertEquals(OperationEnum.SELL, second.getOperation());
        assertEquals(20.0d, second.getUnitCost());
        assertEquals(5000L, second.getQuantity());
    }

    @Test
    void shouldSerializeMany() throws JsonProcessingException {
        OperationResult.Builder builder = new OperationResult.Builder();
        List<OperationResult> results = List.of(builder.tax(345.7).build(), builder.tax(321).build());

        String json = mapper.parse(results);
        assertEquals("[{\"tax\":345.7},{\"tax\":321}]", json);
    }
}