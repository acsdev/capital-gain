package org.challenge.core.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.challenge.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class TaxProcessorTest {

    IMapper mapper = new Mapper();

    ITaxCalculator calculator = new TaxCalculator();

    TaxProcessor taxProcessor = new TaxProcessor(mapper, calculator);

    @ParameterizedTest(name = "Case #{0}")
    @ValueSource(strings = {"1", "2", "3", "4", "5", "6", "7"})
    void shouldDeserializeSingle(String testcase) throws JsonProcessingException {
        final String in = this.getJson(testcase, "in");
        final String outExpected = this.getJson(testcase, "out");

        String outReceived = taxProcessor.execute(in);
        Assertions.assertEquals(outExpected, outReceived);
    }

    @Test
    void shouldFailDouToOverLimit() throws JsonProcessingException {
        final String in = getJson("test-more-sales-than-limit", "in");
        final String outExpected = getJson("test-more-sales-than-limit", "out");

        String outReceived = taxProcessor.execute(in);
        Assertions.assertEquals(outExpected, outReceived);
    }

    private String getJson(String testcase, String inOrOut) {
        String name = String.format("test-cases/%s.%s", testcase, inOrOut);
        return Util.getFileFromResource(name);
    }
}