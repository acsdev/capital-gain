FROM maven:3.8.5-openjdk-11-slim

USER 1000

WORKDIR /tmp

COPY src src
COPY pom.xml .

RUN mvn dependency:resolve
RUN mvn clean compile package
RUN mvn -v
# If you want to check the content of the artifect
# RUN jar tf /tmp/target/core-1.0-SNAPSHOT-jar-with-dependencies.jar
ENTRYPOINT java -jar /tmp/target/core-1.0-SNAPSHOT-jar-with-dependencies.jar /tmp/operationfile/input