# Definitions

- The app is built in the Entity-Control-Boundary Pattern
- The problem solution is coded in the class TaxProcessor
- There is an interface IMapper and class Mapper using as a dependency of TaxProcessor
  - These items were build like this to preserve the single responsibility principle
- There are two custom exceptions to preventing the application starts with an error
- There is a DoubleSerializer to ensure the output will happen the format expected
- There are entities to represent the input and output of the application

# Libraries

- Jackson (To perform deserialization and serialization data)
- Junit5 (To perform simple test and parametrized test)
  - There was no need for a library like Mockito
- PMD (To perform code analyses ) 

# How to run

## With local machine

Make sure that you have mvn 3.8.5 and jdk-11

Use maven to prepare your package
```
mvn clean package
```
To run, execute:
```
java -jar target/core-1.0-SNAPSHOT-jar-with-dependencies.jar <INPUT>
```
where INPUT should be the absolute file path that has the operation


## With docker

First build the image with the command below. **It should take a while at the first time**.

```
docker build -f Dockerfile . -t capital-gain:latest
```

Once the image is done, it is possible to run with the command below
```
docker run --rm -v $(pwd)/docker-volume:/tmp/operationfile --name capital capital-gain:latest
```

### Important

Make sure to have your input file named as **input**. 
This file need to be inside the volume that is being shared. aka `$(pwd)/docker-volume` in the sample above.

# Notes

To check pmd results, execute
```
mvm pmdd:pmd
```
and then open the file **target/site/pmd.html** in any web browser.